import Vue from 'vue'
import Vuex from 'vuex'
import vuexAlong from 'vuex-along'
import router from '@/router/index'

Vue.use(Vuex);

// vuexAlong.watch(['UserName'], true) 

export default new Vuex.Store({
    state:{
        Token:'',
        uid:'',
        UserName:'',
        Status:'',
        currentBookID:'',
        currentBookName:'',
        UserPicPath:''
    },
    mutations:{
        setToken(state,token){
            state.Token=token;
        },
        setUID(state,UID){
            state.uid=UID;
        },
        setUserName(state,username){
            state.UserName=username;
        },
        setStatus(state,Status){
            state.Status=Status;
        },
        setcurrentBookID(state,currentBookID){
            state.currentBookID=currentBookID;
        },
        setcurrentBookName(state,currentBookName){
            state.currentBookName=currentBookName;
        },
        setUserPicPath(state,UserPicPath){
            state.UserPicPath=UserPicPath
        }
    },
    actions:{
        SignOut(){
            vuexAlong.clean()
            router.push('/login')
        }
    },
    plugins:[vuexAlong]
});

