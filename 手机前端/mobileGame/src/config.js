const ServerIP="http://localhost:6243/"

const SetTokenUrl=ServerIP+"auth/SetToken"

const GetAllBooks=ServerIP+"book/GetAllBookID"

const SaveChanges=ServerIP+"book/SaveChanges"

const GetUserInfo=ServerIP+"UserInfo/GetUserInfo"

const GetOldChoose=ServerIP+"book/GetOldChoose"

const GetBookNameByID=ServerIP+"book/GetBookNameByID"

const GetGameQuestions=ServerIP+'Question/GetGameQuestions'

const CheckAns=ServerIP+'Question/CheckAns'

const GetQuestions=ServerIP+'Question/GetQuestions'



export default {
    SetTokenUrl,GetAllBooks,SaveChanges,GetUserInfo,GetOldChoose,GetBookNameByID,
    GetGameQuestions,CheckAns,GetQuestions
}