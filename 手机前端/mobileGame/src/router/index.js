import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/pages/login'
import Menu from '@/pages/menu'
import ChooseBook from '@/pages/ChooseBook'
import StartPK from '@/pages/startpk'
import SelfGame from '@/pages/sgame'
import splaycontext from '@/pages/SelfPlay/splaycontext'
import SPlayResult from '@/pages/SelfPlay/SlefFightResult'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path:'/menu',
      name:'Menu',
      component:Menu
    },
    {
      path:'/choosebook',
      name:'ChooseBook',
      component:ChooseBook
    },{
      path:'/',
      redirect:'/login'
    },
    {
      path:'/startpk',
      name:'startpk',
      component:StartPK
    },{
      path:'/selfgame',
      name:'SelfGame',
      component:SelfGame
    },{
      path:'/splay',
      name:'splay',
      component:splaycontext
    },
    {
      path:'/splayresult',
      name:'splayresult',
      component:SPlayResult
    }
  ]
})
