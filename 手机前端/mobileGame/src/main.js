// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vant from 'vant';
import 'vant/lib/vant-css/index.css';
import axios from 'axios'
import lockr from 'lockr'
import { Toast } from 'vant';
import store from './store'
import config from './config'

Vue.config.productionTip = false

Vue.use(Vant);

Vue.prototype.$http=axios;
Vue.prototype.$lockr=lockr;
Vue.prototype.$toast=Toast;
Vue.prototype.$config=config;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  store
})

router.beforeEach((to,from,next)=>{
  if (to.path!="/login" && to.path!='/') {
    var token =store.state.Token;
  var username=store.state.UserName;

  //获取用户UID
  axios.post(config.GetUserInfo,{
    UserName:username
  }).then(res=>{
    if (res.data.Success==true) {
      // store.state.uid=res.data.Mydata.ID;
      store.commit('setUID',res.data.Mydata.ID)
      // store.state.Status=res.data.Mydata.Status;
      store.commit('setStatus',res.data.Mydata.Status)
      // store.state.currentBookID=(res.data.Mydata.currentBookID===null)?"请选择课本题库":res.data.Mydata.currentBookID
      var currentBookID=(res.data.Mydata.currentBookID===null)?"请选择课本题库":res.data.Mydata.currentBookID;
      store.commit('setcurrentBookID',currentBookID)
      // console.log(store.state.currentBookID)
      store.commit('setUserPicPath',res.data.Mydata.pic)
      next()
    }
    else
    {
      router.push("/login")
    }
  })
  }else
  {
    next()
  }
})