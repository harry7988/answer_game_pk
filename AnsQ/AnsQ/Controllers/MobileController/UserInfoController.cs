﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AnsQ.Models;
using AnsQ.infoViewModel;

namespace AnsQ.Controllers.MobileController
{
    public class UserInfoController : Controller
    {
        // GET: UserInfo
        public JsonResult GetUserInfo(string UserName)
        {
            DataResultCommon dataResultCommon = new DataResultCommon();
            if (!string.IsNullOrEmpty(UserName))
            {
                AnsGameEntities db = new AnsGameEntities();
                db.Configuration.ProxyCreationEnabled = false;
                var result = (from a in db.AUser
                              where a.PlayName == UserName
                              select new { ID = a.ID, UserName = a.PlayName, Status = a.Status,currentBookID=a.CurrentBook,pic=a.UserPic }).FirstOrDefault();
                dataResultCommon.Success = true;
                dataResultCommon.Mydata = result;
                dataResultCommon.Message = "成功";
            }
            else
            {
                dataResultCommon.Success = false;
                dataResultCommon.Message = "失败";
            }
            return Json(dataResultCommon);

        }
    }
}