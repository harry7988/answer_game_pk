﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AnsQ.Models;
using AnsQ.Helper;
using AnsQ.infoViewModel;

namespace AnsQ.Controllers.MobileController
{
    
    public class BookController : Controller
    {
        // GET: Book
        public JsonResult GetAllBookID()
        {
            AnsGameEntities db = new AnsGameEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Book.ToList(),JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveChanges(string uid,string bookid)
        {
            DataResult dataResult = new DataResult();
            if (!string.IsNullOrEmpty(uid) && !string.IsNullOrEmpty(bookid))
            {
                Helper.TSQLHelper.InsertOrUpdateSQL("update AUser set CurrentBook =@BID where ID=@UID", new string[] { "@BID", "@UID" }, new string[] { bookid.Trim(), uid.Trim() });
                dataResult.Message = "ok";
                dataResult.Success = true;
            }
            else
            {
                dataResult.Message = "fail";
                dataResult.Success = false;
            }
            return Json(dataResult);
        }

        public JsonResult GetOldChoose(string uid)
        {
            DataResultCommon dataResultCommon = new DataResultCommon();
            AnsGameEntities db = new AnsGameEntities();
            if (!string.IsNullOrEmpty(uid))
            {
                var result = db.AUser.Where(a => a.ID == uid).Select(d => d.CurrentBook).FirstOrDefault();
                dataResultCommon.Mydata = result;
                dataResultCommon.Message = "成功";
                dataResultCommon.Success = true;
            }
            else
            {
                dataResultCommon.Message = "失败";
                dataResultCommon.Success = false;
            }
            return Json(dataResultCommon);
        }

        public JsonResult GetBookNameByID(string bookid)
        {
            DataResultCommon dataResultCommon = new DataResultCommon();
            if (!string.IsNullOrEmpty(bookid))
            {
                AnsGameEntities db = new AnsGameEntities();
                var result = db.Book.Where(b => b.ID == bookid).Select(a => a.BookName).FirstOrDefault();
                dataResultCommon.Mydata = result;
                dataResultCommon.Message = "成功";
                dataResultCommon.Success = true;
            }
            else
            {
                dataResultCommon.Message = "失败";
                dataResultCommon.Success = false;
            }
            return Json(dataResultCommon);
        }
    }
}