﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AnsQ.Models;
using AnsQ.infoViewModel;

namespace AnsQ.Controllers.MobileController
{
    public class QuestionController : Controller
    {
        // GET: Question
        public JsonResult GetGameQuestions(string bookid)
        {
            DataResultCommon dataResultCommon = new DataResultCommon();

            if (!string.IsNullOrEmpty(bookid))
            {
                AnsGameEntities db = new AnsGameEntities();

                var q = (from t in db.BookPages
                         where t.BookID == bookid
                         orderby Guid.NewGuid()
                         select new
                         {
                             id=t.ID,
                             Q_content = t.Title,
                             Q_Select_A = t.Q_A,
                             Q_Select_B = t.Q_B,
                             Q_Select_C = t.Q_C,
                             Q_Select_D = t.Q_D
                         }).Take(15);

                int iu = 0;
                List<QuestionViewModel> ViewModels = new List<QuestionViewModel>();
                foreach (var item in q)
                {
                    QuestionViewModel questionViewModel = new QuestionViewModel();
                    questionViewModel.QID = iu++;
                    questionViewModel.Gid = item.id;
                    questionViewModel.Q_content = item.Q_content.Replace(" ","_");
                    questionViewModel.Q_Select_A = item.Q_Select_A;
                    questionViewModel.Q_Select_B = item.Q_Select_B;
                    questionViewModel.Q_Select_C = item.Q_Select_C;
                    questionViewModel.Q_Select_D = item.Q_Select_D;
                    ViewModels.Add(questionViewModel);
                }
                dataResultCommon.Success = true;
                dataResultCommon.Message = "成功";
                dataResultCommon.Mydata = ViewModels;
            }
            else
            {
                dataResultCommon.Success = false;
                dataResultCommon.Message = "失败";
            }

            return Json(dataResultCommon);
        }

        public JsonResult CheckAns(string Gid)
        {
            DataResultCommon dataResultCommon = new DataResultCommon();
            if (!string.IsNullOrEmpty(Gid))
            {
                AnsGameEntities db = new AnsGameEntities();
                db.Configuration.ProxyCreationEnabled = false;
                var result = (from a in db.BookPages
                              where a.ID == Gid
                              select a).FirstOrDefault();
                dataResultCommon.Mydata = result;
                dataResultCommon.Message = "成功";
                dataResultCommon.Success = true;
            }
            else
            {
                dataResultCommon.Message = "失败";
                dataResultCommon.Success = false;
            }
            return Json(dataResultCommon);
        }

        public JsonResult GetQuestions(List<string> Gids)
        {
            DataResultCommon dataResultCommon = new DataResultCommon();
            if (Gids!=null && Gids.Count>0)
            {
                List<object> ob = new List<object>();
                foreach (string item in Gids)
                {
                        ob.Add(SearchDB(item));
                }
                dataResultCommon.Mydata = ob;
                dataResultCommon.Message = "成功";
                dataResultCommon.Success = true;
            }
            else
            {
                dataResultCommon.Message = "失败";
                dataResultCommon.Success = false;
            }
            return Json(dataResultCommon);
        }

        private object  SearchDB(string Gid)
        {
            AnsGameEntities db = new AnsGameEntities();
            var result = (from a in db.BookPages
                         where a.ID == Gid
                         select new { QTitle = a.Title, QA = a.Q_A, QB = a.Q_B, QC = a.Q_C, QD = a.Q_D, Ans = a.Q_Ans }).FirstOrDefault();
            QuestionResultSelf questionResultSelf = new QuestionResultSelf();
            questionResultSelf.QTitle = result.QTitle.Replace(' ','_');
            questionResultSelf.QA = result.QA;
            questionResultSelf.QB = result.QB;
            questionResultSelf.QC = result.QC;
            questionResultSelf.QD = result.QD;
            questionResultSelf.Ans = result.Ans.Trim();
            return questionResultSelf;
        }
    }
}