﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnsQ.infoViewModel
{
    public class QuestionViewModel
    {
        public int QID { get; set; }
        public string Gid { get; set; }
        public string Q_content { get; set; }
        public string Q_Select_A { get; set; }
        public string Q_Select_B { get; set; }
        public string Q_Select_C { get; set; }
        public string Q_Select_D { get; set; }
    }
}