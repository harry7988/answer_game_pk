﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnsQ.infoViewModel
{
    public class QuestionResultSelf
    {
        public string QTitle { get; set; }
        public string QA { get; set; }
        public string QB { get; set; }
        public string QC { get; set; }
        public string QD { get; set; }
        public string Ans { get; set; }
    }
}