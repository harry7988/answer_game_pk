﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnsQ.infoViewModel
{
    public class DataResultCommon
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public object Mydata { get; set; }
    }
}